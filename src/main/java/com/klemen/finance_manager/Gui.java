package com.klemen.finance_manager;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.jdesktop.swingx.JXDatePicker;

import net.miginfocom.swing.MigLayout;

public class Gui {

	private static JFrame mainFrame;
	private static JPanel mainPanel;
	private static JPanel formPanel;
	private static JPanel dataPanel;
	
	static JTextField inputTransactionValue;
	static JTextField inputDescription;
	static JTextField textTotalBalance;
	static JTextField textMonthlyBalance;
	static JTextField textMoneyLeft;

	private static JButton buttonSaveTransaction;
	private static JButton buttonRefresh;
	
	private static JLabel labelDescription;
	private static JLabel labelValue;
	private static JLabel labelDate;
	private static JLabel labelBalance;
	private static JLabel labelMonthlyExpense;
	private static JLabel labelMoneyLeft;
	
	static JComboBox<String> inputComboTransactionType;
	private String[] arrayTransactionTypes = {"Income", "Expense"};
	
	static JXDatePicker inputDatePicker;
	private static JMenuBar menuBar;
	private static JMenuItem fileMenuExit;
	private static JMenuItem fileMenuSettings;
	private static JMenu fileMenu;
	private static JMenu editMenu;
	
	
	public void drawGui() throws SQLException {
		ConnectionManager.connect();
		ConnectionManager.createTable();
		mainFrame = new JFrame("Finance manager");
		
		// LAYOUT
		MigLayout northMig = new MigLayout();
		MigLayout southMig = new MigLayout();
		
		// PANELS
		mainPanel = new JPanel(new BorderLayout());
		formPanel = new JPanel(northMig);
		dataPanel = new JPanel(southMig);
		
		// MENU
		menuBar = new JMenuBar();
		fileMenu = new JMenu("File");
		editMenu = new JMenu("Edit");
		fileMenuSettings = new JMenuItem("Settings");
		fileMenuExit = new JMenuItem("Exit");
				
		menuBar.add(fileMenu);
		menuBar.add(editMenu);
		editMenu.add(fileMenuSettings);
		fileMenu.add(fileMenuExit);
				
		// INPUT
		inputTransactionValue = new JTextField();
		inputDescription = new JTextField();
		
		// Balance, Expense, Goal
		textTotalBalance = new JTextField();
		textTotalBalance.setEditable(false);
		textTotalBalance.setText("0");
		textMonthlyBalance = new JTextField();
		textMonthlyBalance.setEditable(false);
		textMonthlyBalance.setText("0");
		textMoneyLeft = new JTextField();
		textMoneyLeft.setEditable(false);
		
		// COMBO BOX
		inputComboTransactionType = new JComboBox<String>(arrayTransactionTypes);
		
		// LABELS
		labelBalance = new JLabel("Total Balance:");
		labelMonthlyExpense = new JLabel("Monthly balance:");
		labelDescription = new JLabel("Description");
		labelValue = new JLabel("Value");
		labelDate = new JLabel("Date");
		labelMoneyLeft = new JLabel("Money left for this month:");
		
		// DATE PICKER
		inputDatePicker = new JXDatePicker();
		inputDatePicker.setFormats("dd.MM.yyyy");
		
		// BUTTONS
		buttonSaveTransaction = new JButton("Save");
		buttonRefresh = new JButton("Refresh");
				
		// ACTION LISTENERS
		buttonSaveTransaction.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Controller.saveButtonAction();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		});
		
		buttonRefresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Controller.refreshButtonAction();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		});
		
		fileMenuSettings.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SettingsWindow.drawSettingsWindow();
				Controller.openSettingsButtonAction();
			}
		});
		
		fileMenuExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					ConnectionManager.closeSQLiteConnection();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
				System.exit(0);
			}
		});
		
		mainFrame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent event) {
				try {
					ConnectionManager.closeSQLiteConnection();
				} catch (SQLException e) {
					e.printStackTrace();
				}
				System.exit(0);
			}
		});
		
		// Add elements to panel
		formPanel.add(labelValue, "skip");
		formPanel.add(labelDescription);
		formPanel.add(labelDate, "wrap");
		
		formPanel.add(inputComboTransactionType);
		formPanel.add(inputTransactionValue);
		formPanel.add(inputDescription);
		formPanel.add(inputDatePicker, "wrap");
		
		formPanel.add(buttonSaveTransaction, "wrap");
		
		formPanel.add(buttonRefresh, "wrap");
		
		dataPanel.add(labelBalance);
		dataPanel.add(textTotalBalance);
		dataPanel.add(labelMoneyLeft);
		dataPanel.add(textMoneyLeft, "wrap");
		
		dataPanel.add(labelMonthlyExpense);
		dataPanel.add(textMonthlyBalance);

		// Set dimensions
		inputTransactionValue.setPreferredSize(new Dimension(85, 23));
		inputDescription.setPreferredSize(new Dimension(280, 23));
		buttonSaveTransaction.setPreferredSize(new Dimension(90, 23));
		buttonRefresh.setPreferredSize(new Dimension(90, 23));
		textTotalBalance.setPreferredSize(new Dimension(85, 23));
		textMonthlyBalance.setPreferredSize(new Dimension(85, 23));
		textMoneyLeft.setPreferredSize(new Dimension(85, 23));
		
		mainPanel.add(formPanel, BorderLayout.NORTH);
		mainPanel.add(dataPanel, BorderLayout.SOUTH);
		mainFrame.add(mainPanel);
		
		// Frame settings
		mainFrame.setJMenuBar(menuBar);
		mainFrame.setResizable(false);
		mainFrame.setLocationRelativeTo(null);
		mainFrame.setVisible(true);
		mainFrame.pack();
		
		mainFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		
	}
	
}
