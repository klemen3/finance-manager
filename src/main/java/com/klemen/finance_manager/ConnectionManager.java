package com.klemen.finance_manager;

import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import javax.swing.JOptionPane;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

public class ConnectionManager {

	private static Connection connection               = null;
	private static Statement statement                 = null;
	private static PreparedStatement preparedStatement = null;
	private static ResultSet resultSet                 = null;

	// Queries
	private static String createTransactionTableQuery;
	private static String createSettingsTableQuery;
	private static String insertTransactionQuery;
	private static String selectTotalBalanceQuery;
	private static String selectMonthlyBalanceQuery;
	private static String selectMonthlyBudgetQuery;
	private static String getBudgetSettingQuery;
	private static String checkIfSettingsExistQuery;
	private static String updateSettingsQuery;
	private static String insertSettingsQuery;
	private static String selectDebugQuery;
	
	// Rounding format
	private static DecimalFormat roundingFormat = new DecimalFormat("#.##", new DecimalFormatSymbols(Locale.US));
	
	
	// Inject properties and set rounding mode
	static {		
		PropertiesConfiguration config;
		try {
			config = new PropertiesConfiguration("application.properties");
			createTransactionTableQuery = config.getString("createTransactionTableQuery");
			createSettingsTableQuery    = config.getString("createSettingsTableQuery");
			insertTransactionQuery      = config.getString("insertTransactionQuery");
			selectTotalBalanceQuery     = config.getString("selectTotalBalanceQuery");
			selectMonthlyBalanceQuery   = config.getString("selectMonthlyBalanceQuery");
			selectMonthlyBudgetQuery    = config.getString("selectMonthlyBudgetQuery");
			getBudgetSettingQuery       = config.getString("getBudgetQuery");
			checkIfSettingsExistQuery   = config.getString("checkIfSettingsExistQuery");
			updateSettingsQuery         = config.getString("updateSettingsQuery");
			insertSettingsQuery         = config.getString("insertSettingsQuery");
			selectDebugQuery            = config.getString("selectDebugQuery");
			System.out.println("Config loaded.");
			
			roundingFormat.setRoundingMode(RoundingMode.HALF_EVEN);
		} catch (ConfigurationException e) {
			e.printStackTrace();
		}
	}
	
	
	// Connect to database
	public static void connect() throws SQLException {
		try {
			Class.forName("org.sqlite.JDBC");
			connection = DriverManager.getConnection("jdbc:sqlite:financeManager_db");
			System.out.println("connected to database...");
		} catch (ClassNotFoundException e) {
			JOptionPane.showMessageDialog(null, e);;
		}
	}

	// Create tables when starting application for the first time
	public static void createTable() throws SQLException {	
		statement = connection.createStatement();
		statement.executeUpdate(createTransactionTableQuery);
		statement.executeUpdate(createSettingsTableQuery);
		System.out.println("Table created.");
	}

	// Insert transaction
	public static void insert(double amount, String description, String date) throws SQLException {
		preparedStatement = connection.prepareStatement(insertTransactionQuery);
		preparedStatement.setDouble(1, amount);
		preparedStatement.setString(2, description);
		preparedStatement.setString(3, date);
		preparedStatement.executeUpdate();

		System.out.println("Row inserted");
	}

	// Calculate total balance from db and return string of rounded result
	public static String selectTotalBalance() throws SQLException {	
		resultSet = statement.executeQuery(selectTotalBalanceQuery);
		double totalBalance = 0;
		while (resultSet.next()) {
			totalBalance = resultSet.getDouble(1);
		}
		String totalBalanceStr = roundingFormat.format(totalBalance);
		System.out.println("Total balance: " + totalBalanceStr);
		return totalBalanceStr;

	}

	// Calculate monthly balance from db and return string of rounded result
	public static String selectMonthlyBalance() throws SQLException {	
		resultSet = statement.executeQuery(selectMonthlyBalanceQuery);
		double monthlyBalance = 0;
		while (resultSet.next()) {
			monthlyBalance = resultSet.getDouble(1);
		}
		String monthlyBalanceStr = roundingFormat.format(monthlyBalance);
		System.out.println("Monthly balance: " + monthlyBalanceStr);
		return monthlyBalanceStr;
	}
	
	// Calculate monthly budget left from db and return string of rounded result
	public static String monthlyBudgetLeft() throws SQLException {	
		resultSet = statement.executeQuery(selectMonthlyBudgetQuery);
		double monthlyExpense = 0;
		while (resultSet.next()) {
			monthlyExpense = resultSet.getDouble(1);
		}
		// monthlyExpense je negativen, zato pristejemo
		double budget = Double.parseDouble(getMonthlyBudgetSetting());
		double moneyLeft = budget + monthlyExpense;
		
		String moneyLeftStr = roundingFormat.format(moneyLeft);
		System.out.println("Spending money left for this month: " + moneyLeftStr);
		return moneyLeftStr;
	}
	
	// Get monthly budget setting from db and return string of rounded result
	public static String getMonthlyBudgetSetting() throws SQLException {
		double budgetSetting = 0;
		resultSet = statement.executeQuery(getBudgetSettingQuery);
		while (resultSet.next()) {
			budgetSetting = resultSet.getDouble(1);
		}
		String budgetSettingStr = roundingFormat.format(budgetSetting);
		return budgetSettingStr;
	}
	
	// Save budget to database
	public static void insertSettings(double budget) {
		try {
			resultSet = statement.executeQuery(checkIfSettingsExistQuery);
			if (resultSet.next()) {
				preparedStatement = connection.prepareStatement(updateSettingsQuery);
				preparedStatement.setDouble(1, budget);
				preparedStatement.executeUpdate();
			} else {
				preparedStatement = connection.prepareStatement(insertSettingsQuery);
				preparedStatement.setDouble(1, budget);
				preparedStatement.executeUpdate();
			}
			
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "SQL exception occured: " + e);
		}
	}

	// Select * - for debugging
	public static void selectDebug() throws SQLException {
		resultSet = statement.executeQuery(selectDebugQuery);
		while (resultSet.next()) {
			System.out.print(resultSet.getInt(1) + ", ");
			System.out.print(resultSet.getString(2) + ", ");
			System.out.print(resultSet.getInt(3) + ", ");
			System.out.println(resultSet.getString(4) + ", ");
		}
	}

	// Close database connection
	public static void closeSQLiteConnection() throws SQLException {
		statement.close();
		if (preparedStatement != null) {
			preparedStatement.close();
		}
		connection.close();
		System.out.println("database connection closed.");
	}

}
