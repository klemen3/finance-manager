package com.klemen.finance_manager;

import java.sql.SQLException;

public class Runner {

	public static void main(String[] args) throws SQLException {
		Gui gui = new Gui();
		gui.drawGui();
		Gui.textTotalBalance.setText(String.valueOf(ConnectionManager.selectTotalBalance()));
		Gui.textMonthlyBalance.setText(String.valueOf(ConnectionManager.selectMonthlyBalance()));
		Gui.textMoneyLeft.setText(String.valueOf(ConnectionManager.monthlyBudgetLeft()));
		
	}
	
}

