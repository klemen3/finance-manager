package com.klemen.finance_manager;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;

public class Controller {
	
	public static void saveButtonAction() throws SQLException {

		String type = Gui.inputComboTransactionType.getSelectedItem().toString();
		String valueText = Gui.inputTransactionValue.getText();

		double value;

		try {
			value = Double.parseDouble(valueText);
			String description = Gui.inputDescription.getText();

			JFormattedTextField editor = Gui.inputDatePicker.getEditor();
			Date dateIDP = (Date) editor.getValue();
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			String date = format.format(dateIDP);
			System.out.println(value);
			if (value < 0) {
				JOptionPane.showMessageDialog(null, "Input 'Value' must be a POSITIVE real number.");
			} else {
				if (type.equals("Income")) {
					System.out.println("v1: " + value);
					ConnectionManager.insert(value, description, date);
				} else {
					System.out.println("v2: " + value);
					ConnectionManager.insert(-value, description, date);
				}
			}
			// Set text in Gui for TotalBalance, MonthlyBalance, MoneyLeft
			Gui.textTotalBalance.setText(String.valueOf(ConnectionManager.selectTotalBalance()));
			Gui.textMonthlyBalance.setText(String.valueOf(ConnectionManager.selectMonthlyBalance()));
			Gui.textMoneyLeft.setText(String.valueOf(ConnectionManager.monthlyBudgetLeft()));
			
		} catch (Exception e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "Input 'Value' must be a real number");
		}
	}

	public static void refreshButtonAction() throws SQLException {
		Gui.textTotalBalance.setText(String.valueOf(ConnectionManager.selectTotalBalance()));
		Gui.textMonthlyBalance.setText(String.valueOf(ConnectionManager.selectMonthlyBalance()));
		Gui.textMoneyLeft.setText(String.valueOf(ConnectionManager.monthlyBudgetLeft()));
		ConnectionManager.selectDebug();
	}
	
	// klik na gumb Save v Settings oknu
	// poklice funkcijo iz ConnectionManager, ki iz podatkov, ki so v bazi,
	// izracuna, koliko denarja je se ostalo za ta mesec
	public static void saveSettingsButtonAction() {
		try {
			double budget = Double.parseDouble(SettingsWindow.inputMonthlyGoal.getText());
			if (budget <= 0) {
				JOptionPane.showMessageDialog(null, "Input 'Monthly budget' must be a POSITIVE real number.");
			} else {
				// Save budget to db
				ConnectionManager.insertSettings(budget);
				// Get budget from db and set text in Gui
				Gui.textMoneyLeft.setText(String.valueOf(ConnectionManager.monthlyBudgetLeft()));
			}
		} catch (Exception e2) {
			e2.printStackTrace();
			JOptionPane.showMessageDialog(null, "Input 'Monthly budget' must be a real number");
		}
	}

	// Set settings input fields to already defined values if they exist,
	// otherwise to some initial values.
	public static void openSettingsButtonAction() {
		try {
			SettingsWindow.inputMonthlyGoal.setText(String.valueOf(ConnectionManager.getMonthlyBudgetSetting()));
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "SQL exception occured: " + e);
			e.printStackTrace();
		}
		
	}
	
}
